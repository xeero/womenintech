import {HttpService} from "../http/HttpService";
import {User} from "../models/User";
import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {
    constructor(private _httpService: HttpService<User>){
    }

    Login(user: User): Promise<User[]>{
        return this._httpService.post("/userauth", [user]);
    }
}