import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../login/login.service';
import {User} from '../models/User';
import {HttpService} from "../http/HttpService";

@Component({
    selector:'wit-login',
    templateUrl: './app/login/loginTemplate.html',
    providers: [HttpService]
})

export class LoginComponent {
    private _loginService: LoginService;
    private _router: Router;

    RegistrationErrorMsg: any;
    user = new User();
    
    constructor(loginService: LoginService, router: Router){
        this._loginService = loginService;
        this._router = router;
    }
    
    SubmitRegistration() {
        this._loginService.Login(this.user).then(users => {
            this.user = users[0];
            if(this.user.LoggedIn) {
                this._router.navigate(['sessions']);
            }
        })
        .catch(error => {
            this.RegistrationErrorMsg = error._body;
        });
    }

}