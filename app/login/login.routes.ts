
import { Routes } from '@angular/router';
import { SessionsComponent } from '../sessions/sessions.component';

// Route Configuration
export const loginRoutes: Routes = [
  { path: 'sessions', component: SessionsComponent }
];