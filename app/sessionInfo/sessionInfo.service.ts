import {HttpService} from "../http/HttpService";
import { Injectable } from '@angular/core';
import {SessionInfo} from '../models/SessionInfo';

@Injectable()
export class SessionInfoService {
    constructor(private _httpService: HttpService<SessionInfo>){
    }

    GetSessionInfo(id: string): Promise<Array<SessionInfo>>{
        return this._httpService.get("/session/" + id);
    }
}