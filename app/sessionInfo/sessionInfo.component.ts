import {Component, OnInit} from '@angular/core';
import { SessionInfo } from '../models/SessionInfo';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {SessionInfoService} from '../sessionInfo/sessionInfo.service';

@Component({
    selector:'wit-sessionInfo',
    templateUrl:'./app/sessionInfo/sessionInfoTemplate.html'
})

export class SessionInfoComponent implements OnInit {

    public sessionInfo: SessionInfo = new SessionInfo();
    GetSessionInfoErrorMsg: any;
    private id: string;

    constructor(private _sessionInfoService: SessionInfoService, private route: ActivatedRoute){
    }
    
    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            this.id = params['id']; 
        });
        this._sessionInfoService.GetSessionInfo(this.id).then(sessionInfo => {
            this.sessionInfo = sessionInfo;
        })
        .catch(error => {
            this.GetSessionInfoErrorMsg = error._body;
        });
    }
}