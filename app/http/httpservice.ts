import {iHttpService} from "../http/ihttpservice";
import {DbCollection} from "../http/DbCollection";
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HttpService<T extends DbCollection> implements iHttpService<T> {
    private _apiHost: string = "http://localhost:4000";
    private _headers : Headers;
    private _options : RequestOptions;

    constructor(private _http: Http){
        this._headers = new Headers({
                'Content-Type': 'application/json'
            });
        this._options = new RequestOptions({ headers: this._headers });
    }

    get(url: string, collection_name?: string, collection_id?: string): Promise<Array<T>> {
        var data = new Array<T>();
        return this._http.get(this._apiHost + url, this._options)
            .toPromise()
            .then(response => response.json() as Array<T>)
            .catch(this.handleError);
    }
    post(url: string, collection: Array<T>): Promise<Array<T>> {
        var data = new Array<T>();
        return this._http.post(this._apiHost + url, JSON.stringify(collection), this._options)
            .toPromise()
            .then(response => response.json() as Array<T>)
            .catch(this.handleError);
    }
    put(url: string, collection_value: T): Promise<T> {
        return this._http.post(this._apiHost + url, collection_value, this._options)
            .toPromise()
            .then(response => response.json() as T)
            .catch(this.handleError);
    }
    delete(url: string, collection_value:T): Promise<string> {
        return this._http.delete(this._apiHost + url, collection_value)
            .toPromise()
            .then(response => "Successfully deleted!")
            .catch(this.handleError);
    }
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}