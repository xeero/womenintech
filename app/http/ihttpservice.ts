export interface iHttpService<T> {
    get(url: string, collection_name?: string, collection_id?: string): Promise<Array<T>>;
    post(url: string, collection: Array<T>): Promise<Array<T>>;
    put(url: string, collection_value: T): Promise<T>;
    delete(url: string, collection:T):Promise<string>;
}

