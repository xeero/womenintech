import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Session} from '../models/Session';
import {SessionsService} from '../sessions/sessions.service';

@Component({
    selector:'wit-sessions',
    templateUrl:'./app/sessions/sessionsTemplate.html'
})

export class SessionsComponent implements OnInit {
    private _router : Router;
    public Sessions: Array<Session>;

    GetSessionsErrorMsg: any;

    constructor(private _sessionsService: SessionsService,router: Router){
        this._router = router;
    }
    
    ngOnInit() {
        this._sessionsService.GetSessions().then(sessions => {
            this.Sessions = sessions;
        })
        .catch(error => {
            this.GetSessionsErrorMsg = error._body;
        });
    }

    SeeDetails(session: Session){
        this._router.navigate(['/sessionInfo', session.Id]);
    }
}