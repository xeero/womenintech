import { Routes } from '@angular/router';
import { SessionInfoComponent } from '../sessionInfo/sessionInfo.component';

// Route Configuration
export const SessionsRoutes: Routes = [
    { path: 'sessionInfo/:id', component: SessionInfoComponent }
];