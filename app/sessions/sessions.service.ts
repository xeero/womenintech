import {HttpService} from "../http/HttpService";
import { Injectable } from '@angular/core';
import {Session} from '../models/Session';

@Injectable()
export class SessionsService {
    constructor(private _httpService: HttpService<Session>){
    }

    GetSessions(): Promise<Array<Session>>{
        return this._httpService.get("/sessions");
    }
}