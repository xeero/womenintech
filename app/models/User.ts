export class User {
    public Id: number;
    public FirstName: string;
    public LastName: string;
    public Email: string;
    public Password: string;
    public LoggedIn: boolean;
}