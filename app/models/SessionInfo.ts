export class SessionInfo {
    public Id: number;
    public Speaker: string;
    public Start: string;
    public End: string;
    public Topic: string;
    public Description: string;
}