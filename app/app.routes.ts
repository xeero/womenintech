import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import { loginRoutes } from './login/login.routes';
import { SessionsRoutes } from './sessions/sessions.routes';

// Route Configuration
export const routes: Routes = [
  {
    path: 'registerlogin',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'registerlogin',
    pathMatch: 'full'
  },
    // Add login routes form a different file
  ...loginRoutes,
  ...SessionsRoutes
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
