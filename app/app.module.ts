import { NgModule }      from '@angular/core';
import { HttpModule }    from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {SessionsComponent} from './sessions/sessions.component';
import {SessionsService} from './sessions/sessions.service';
import {SessionInfoService} from './sessionInfo/sessionInfo.service';
import {SessionInfoComponent} from './sessionInfo/sessionInfo.component';
import { FormsModule }   from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoginService} from './login/login.service';
import { routing } from './app.routes';
import {HttpService} from "./http/HttpService";

@NgModule({
  imports:      [ BrowserModule, FormsModule, NgbModule, routing, HttpModule ],
  declarations: [AppComponent, LoginComponent, SessionsComponent,SessionInfoComponent],
  bootstrap: [AppComponent, LoginComponent],
  providers: [LoginService, HttpService, SessionsService, SessionInfoService]
})

export class AppModule { }
