module.exports = function(config) {
    config.set({
        basePath: '.',
        plugins: [
            // 'karma-jasmine',
            // 'karma-coverage',
            // 'karma-chrome-launcher',
            // 'karma-typescript'
            require('karma-jasmine'),
            require('karma-coverage'),
            require('karma-typescript'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'), // click "Debug" in browser to see it
            require('karma-htmlfile-reporter') // crashing w/ strange socket error
        ],
        frameworks: ['jasmine', "karma-typescript"],
        proxies: {
            // required for component assests fetched by Angular's compiler
            '/src/': '/base/src/'
        },
        files: [
            // System.js for module loading
            'node_modules/systemjs/dist/system.src.js',

            // Polyfills
            'node_modules/core-js/client/shim.js',
            'node_modules/reflect-metadata/Reflect.js',

            // zone.js
            'node_modules/zone.js/dist/zone.js',
            'node_modules/zone.js/dist/long-stack-trace-zone.js',
            'node_modules/zone.js/dist/proxy.js',
            'node_modules/zone.js/dist/sync-test.js',
            'node_modules/zone.js/dist/jasmine-patch.js',
            'node_modules/zone.js/dist/async-test.js',
            'node_modules/zone.js/dist/fake-async-test.js',

            {pattern: 'systemjs.config.js', included: true, watched: false},
            {pattern: 'karma-test-shim.js', included: false, watched: false},
            {pattern: 'app/**/*.ts', included: false, watched: true},
            {pattern: 'tests/**/*.spec.ts', included: true, watched: true}
            
        ],
        exclude: [
        ],
        preprocessors: {
             "**/*.ts": ["karma-typescript"]
        },
        typescriptPreprocessor: {
            options: {
                sourceMap: true, // generate source maps
                noResolve: false // enforce type resolution
            },
            transformPath: function(path) {
                return path.replace(/\.ts$/, '.js');
            }
        },
        reporters: ["progress", "karma-typescript"],
        karmaTypescriptConfig: {
            reports:
            {
                "html": "coverage",
                "text-summary": "" // destination "" will redirect output to the console
            }
        },
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        singleRun: false
    });
};